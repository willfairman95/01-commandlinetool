//
//  Array.hpp
//  CommandLineTool
//
//  Created by Will Fairman on 09/10/2015.
//  Copyright © 2015 Tom Mitchell. All rights reserved.
//

/** Class for creating a dynamic array*/

#ifndef Array_hpp
#define Array_hpp

#include <stdio.h>

class Array
{
public:
    //constructor
    Array();
    
    //deconstructor
    ~Array();
    
    //add new items to end of array
    void add (float itemValue);
    
    //return the item at the index
    float get (int index) const;
    
    //return the number of items currently in array
    int getSize();
    
private:
    int size;
    float* pfloat;
};

#endif /* Array_hpp */
